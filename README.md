# n-grams - Bakalářská práce

- [n-grams - Bakalářská práce](#n-grams-bakalarska-prace)
  - [**Assignment**](#assignment)
  - [**Abstract**](#abstract)
  - [**Technology**](#technology)
  - [**Year**](#year)
  - [**Implementation**](#implementation)
    - [**B+ Tree**](#b-tree)
    - [**R-Tree**](#r-tree)
  - [**Presentations**](#presentations)
  - [**Text**](#text)

### **Assignment**
Implementace efektivních datových struktur - Naimplementujte vhodné datové struktury použitelné pro efektivní vyhledávání v datech. Tato data mohou být například n-gramy extrahované z textů, nebo sekvence DNA. Následně tyto datové struktury otestujte na vhodné datové kolekci a srovnejte výsledky. Pro implementaci použijte programovací jazyk C, nebo C++.

---
### **Abstract**
Práce se zabývá stromovými datovými strukturami. Především B+ stromy a R–stromy,
které jsou vhodné pro ukládání n–gramů a zejména pak k jejich vyhledávání. N–gramy jsou části textu používané například pro detekci plagiátorství, porovnání DNA, detekci spamu či překladu z jednoho jazyka do druhého. Práce se také zabývá implementací těchto dvou stromových struktur a jejich otestování na vhodných testovacích datech.

---
### **Technology**
C++

---
### **Year**
2014

---
### **Implementation**
#### **B+ Tree**

Ukázka štěpení B+ stromu při vložení n-gramu s klíčem 14  
![](./README/B+Tree_Example.png)  

Ukázka výstupu metody UkazStrom()  
![](./README/B+Tree_UkazStrom.png)  

Ukázka výstupu metody Vypis()  
![](./README/B+Tree_Vystup.png)  

#### **R-Tree**

Ukázka stromové struktury R–stromu  
![](./README/R-tree_T.png)  

Jedna z možných planárních reprezentací dat R–stromu  
![](./README/R-tree_S.png)  

Ukázka výstupu metody UkazStrom()  
![](./README/R_Tree_UkazStrom.png)  

Ukázka výstupu metody VypisPlus()  
![](./README/r_Tree_VystupPlus.png)  

---
### **Presentations**  
[Implementace efektivních datových struktur pptx](/README/Implementace%20efektivn%C3%ADch%20datov%C3%BDch%20struktur.pptx)  
[Implementace efektivních datových struktur pdf](/README/Implementace%20efektivn%C3%ADch%20datov%C3%BDch%20struktur.pdf)

### **Text**  
[bakalarka pdf](/README/bakalarka.pdf)